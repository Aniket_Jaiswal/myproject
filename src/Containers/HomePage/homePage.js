import React, {useEffect} from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { fetchProductType, fetchAllProducts } from '../../Redux/actions/product';
import { fetchCarauselItems } from '../../Redux/actions/carausel';

import Section from '../../Components/Section/section';
import styled from 'styled-components';
import useProducts from '../../helpers/utils/useProducts';
import CartPage from '../CartPage/CartPage';
import Modal from '../../Components/Modal/modal';
import Carausel from '../../Components/Carausel/Carausel'

const HomePageWrapper = styled.main`
    max-width: 80%;
    margin: auto;
`;
function HomePage(){

    const dispatch = useDispatch();
    const {filteredProductType, handleProduct} = useProducts();
    const openCart = useSelector((state) => state.cart.openCart);
    
    useEffect(()=>{
        dispatch(fetchProductType());
        dispatch(fetchAllProducts());
        dispatch(fetchCarauselItems())
    },[])
    
    return(
        <>
            <HomePageWrapper>
            <Carausel />
            {
                (filteredProductType || []).map((item,index)=>{
                    return <Section key={item.id}  index={index} handleProduct={handleProduct} {...item}/>
                })
            }
            </HomePageWrapper>
            {openCart &&
                <Modal>
                    <CartPage header={true} />
                </Modal>
            }
        </>
        
    )
}

export default HomePage;