import React from 'react';
import styled from 'styled-components';
import { signUpFields } from './signUpFields';
import useForm from '../../helpers/utils/useForm';
import useError from '../../helpers/utils/useError';
import Button from '../../Components/Button/button';
import InputField from '../../Components/InputField/inputField';
import Form from '../../Components/Form/Form';

const SignUpWrapper = styled.main`
    max-width: 60%;
    margin: auto;
    display: flex;
    justify-content: space-between;
    padding-top: 3rem;

    @media (max-width: 480px){
        flex-direction: column;
        max-width: 90%;
    }
`;


const SignUpContainer = styled.div`
    width: 100%;
    display: flex;
    flex-direction: column;
    & button {
        margin-top: 10px;
        width: 100%;
    }
`;

const SignUpHeader = styled.h3`
    font-size: 1.5rem;
    text-align: left;
`;
const SignUpInfo = styled.h3`
    font-size: .9rem;
    margin-top: 1rem;
    text-align: left;
`;

export default function SignUp() {
    const initialFormValue = {
        firstName: '',
        lastName: '',
        email: '',
        password: '',
        confirmPassword: '',
    }
    const { fieldValue, handleForm } = useForm(initialFormValue);
    const { error, handleError } = useError(initialFormValue);
    
    const handleClick = () => {
        console.log(fieldValue)
    }

    return (
        <SignUpWrapper>
            <SignUpContainer>
                <SignUpHeader>SignUp</SignUpHeader>
                <SignUpInfo>We do not share your personal details</SignUpInfo>
            </SignUpContainer>
            <SignUpContainer>
                <Form>
                {
                    (signUpFields || []).map(field => {
                        return (
                            <InputField 
                                label={field.label}
                                type={field.type}
                                name={field.name}
                                key={field.id}
                                handleForm = { handleForm }
                                value = {fieldValue[field.name] || field.value}
                                error={error[field.name]}
                                regex={field.regex}
                                handleError={handleError}
                                autoComplete={field.autoComplete}
                                required={field.required}
                            />
                        )
                    })
                }
                </Form>
                
                <Button onClick={handleClick}>SignUp</Button>
            </SignUpContainer>
        </SignUpWrapper>
    )
}