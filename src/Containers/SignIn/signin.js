

import React from 'react';
import styled from 'styled-components';
import { signInFields } from './signInFields';
import useForm from '../../helpers/utils/useForm';
import useError from '../../helpers/utils/useError';
import Button from '../../Components/Button/button';
import InputField from '../../Components/InputField/inputField';
import Form from '../../Components/Form/Form';


const SignInWrapper = styled.main`
    max-width: 60%;
    margin: auto;
    display: flex;
    justify-content: space-between;
    padding-top: 3rem;
    @media (max-width: 480px){
        flex-direction: column;
        max-width: 90%;
    }
`;


const SignInContainer = styled.div`
    display: flex;
    flex-direction: column;
    & button {
        margin-top: 10px;
        width: 100%;
    }

`;

const SignInHeader = styled.h3`
    font-size: 1.5rem;
    text-align: left;
`;
const SignInInfo = styled.h3`
    font-size: .9rem;
    margin-top: 1rem;
    text-align: left;
`;

export default function SignIn() {
    const initialFormValue = {
        email: '',
        password: '',
    }
    const { fieldValue, handleForm } = useForm(initialFormValue);
    const { error, handleError } = useError(initialFormValue);

    const handleClick = () => {
        console.log(fieldValue)
    }

    return (
        <SignInWrapper>
            <SignInContainer>
                <SignInHeader>Login</SignInHeader>
                <SignInInfo>Get Access to your Orders, Wishlist and Recommendations</SignInInfo>
            </SignInContainer>
            <SignInContainer>
                <Form>

                    {
                        (signInFields || []).map(field => {
                            return (
                                <InputField
                                    label={field.label}
                                    type={field.type}
                                    name={field.name}
                                    handleForm={handleForm}
                                    value={fieldValue[field.name] || field.value}
                                    error={error[field.name]}
                                    regex={field.regex}
                                    handleError={handleError}
                                    autoComplete={field.autoComplete}
                                    required={field.required}
                                />
                            )
                        })
                    }
                </Form>
                <Button onClick={handleClick}>SignIn</Button>


            </SignInContainer>
        </SignInWrapper>
    )
}