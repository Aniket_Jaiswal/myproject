import React from 'react';
import useProducts from '../../helpers/utils/useProducts';
import useMediaQuery from '../../helpers/utils/useMediaQuery';
import { useSelector } from 'react-redux';
import styled from 'styled-components';
import ProductCard from '../../Components/ProductCard/productCard';
import SideBar from '../../Components/SideBar/siderBar';
import DropDown from '../../Components/DropDown/dropDown';
import CartPage from '../CartPage/CartPage';
import Modal from '../../Components/Modal/modal';

const ProductPageWrapper = styled.main`
    max-width: 80%;
    margin: auto;
    display: flex;
    flex-direction: row;
    @media (max-width: 480px){
        flex-direction: column;
        max-width: 100%;
    }
    @media (min-width:481px) and (max-width: 768px) {
        max-width: 100%;
    }
`;

export default function ProductPage() {
    const { filteredProducts, filteredProductType, handleProduct } = useProducts();
    const isMobile = useMediaQuery("(max-width: 480px)");
    const isTab = useMediaQuery("(min-width:481px) and (max-width: 768px)");
    const openCart = useSelector((state) => state.cart.openCart);

    return (
        <>
            <ProductPageWrapper>
                {isMobile ? 
                    <DropDown 
                    filteredProductType={filteredProductType} 
                    handleProduct={handleProduct} 
                    />
                    : <SideBar filteredProductType={filteredProductType} handleProduct={handleProduct} />}


                <div className='container-fluid' style={{ minWidth: '80%' }}>
                    <div className='row'>
                        {(filteredProducts || []).map(item => {

                            return (
                                <div className={isTab ? 'col-sm-6' : 'col-sm-4'} key={item.id}>
                                    <ProductCard key={item.id} {...item} />
                                </div>
                            )

                        })}
                    </div>

                </div>
            </ProductPageWrapper>
            {openCart &&
                <Modal>
                    <CartPage header={true} />
                </Modal>
            }
        </>
    )
}