import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import styled from 'styled-components';
import CartSection from '../../Components/CartSection/cartSection';
import { countTotalItems, countTotalPrice } from '../../helpers/cartPrice';
import useProducts from '../../helpers/utils/useProducts';
import { toggleCart } from '../../Redux/actions/cart';

const CartPageWrapper = styled.main`
    max-width: 100%;
    background: #ddd;
    padding-bottom: 2rem;

    @media (max-width: 480px){
        padding-top: 2rem;
        min-height: 6rem;
    }
    @media (min-width:481px) and (max-width: 768px) {
        padding-top: 2rem;
        min-height: 6rem;
    }
`;

const CartPageHeader = styled.h6`
    font-size: .9rem;
    background-color: #fff;
    padding: 1rem 1rem;
    text-align: left;
    font-weight: bold;
    margin-bottom: 2rem;
`;

const CartItemsWrapper = styled.div`
    background-color: #fff;
    margin-bottom: 2rem;
`;

const CartPageDiscountWrapper = styled.div`
    display: flex;
    background-color: #fff;
    max-width: 95%;
    margin: auto;
    justify-content: flex-start;
`;

const CartPageFooterContent = styled.div`
    display: flex;
    background: #d42559;
    justify-content: space-around;
    max-width: 95%;
    margin: auto;
    color: white;
    cursor: pointer;
`;

const CartPageMessage = styled.p`
    font-size: 0.9rem;
    font-weight: ${props=> props.bold ? 'bold': '' }
`;

const NoItemInCart = styled.div`
    display: flex;
    flex-direction : column;
    align-items: center;
    justify-content : center;
    min-height: 60vh;
`;

const CartPageFooterWrapper = styled.div`
    padding-top: 1rem;
`;

export default function CartPage({header}) {
    const dispatch = useDispatch();
    const cartItems = useSelector((state) => state.cart.cartItems);
    const { handleProduct } = useProducts();
    const totalItems = countTotalItems(cartItems);
    const totalPrice = countTotalPrice(cartItems);

    return (
        <>
            <CartPageWrapper>
                { !header && <CartPageHeader>{`My Cart(${totalItems} items)`}</CartPageHeader>}
                {
                    totalItems > 0 &&
                    <>
                        <CartItemsWrapper>
                            {
                                (cartItems || []).map(item => {
                                    return <CartSection key={item.id} {...item} />
                                })
                            }
                        </CartItemsWrapper>
                        <CartPageDiscountWrapper>
                            <img src='static/images/lowest-price.png' alt='cart-image'/>
                            <CartPageMessage>You won't find it cheaper anywhere</CartPageMessage>
                        </CartPageDiscountWrapper>
                    </>
                }
            </CartPageWrapper>
            {totalItems < 1 &&
                <NoItemInCart>
                    <CartPageMessage bold={true} >No items in your cart</CartPageMessage>
                    <CartPageMessage>Your favourite items are just a click away</CartPageMessage>
                </NoItemInCart>
            }
            <CartPageFooterWrapper>
                {totalItems > 0 && <CartPageMessage>Promo code can be applied on payment page</CartPageMessage>}
                <CartPageFooterContent>
                    {totalItems > 0 ?
                        <>
                            <CartPageMessage onClick={()=> {handleProduct();dispatch(toggleCart(false))}}>Proceed to Checkout</CartPageMessage>
                            <CartPageMessage>{`Rs. ${totalPrice}`}</CartPageMessage>
                        </>
                        : <CartPageMessage onClick={()=> {handleProduct();dispatch(toggleCart(false))}}>Start Shopping</CartPageMessage>
                    }
                </CartPageFooterContent>
            </CartPageFooterWrapper>

        </>
    )
}