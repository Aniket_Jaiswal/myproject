import { actions } from '../actionTypes/actionConstants';
import { api } from '../../helpers/axiosInstance';

export const upateCart = (id, operation) => {
    return (dispatch, getState) => {
        const productList = getState().products.allProducts;
        const cartItems = getState().cart.cartItems;
        const currentItem = productList.find(({ id: currentItemId }) => currentItemId === id);
        const isItemInCart = cartItems.find(({ id }) => id === currentItem.id);
        let finalItem = [];
        if (!isItemInCart) {
            finalItem = [...cartItems, { ...currentItem, count: 1 }]
        } else {
            finalItem = cartItems.map(item => {
                if (item.id === currentItem.id) {
                    operation === 'add' ? item.count = item.count + 1 : item.count = item.count - 1
                }
                return item;
            }).filter(({ count }) => count)
        }
        api.post('/addToCart', id)
            .then(res => {
                if (res) {
                    dispatch({
                        type: actions.UPDATE_TO_CART,
                        payload: finalItem
                    })
                }
            })

    }
}

export const toggleCart = (payload) => {
    return{
        type: actions.OPEN_CART,
        payload: payload,
    }
}

// export const upateCart = (id, operation) => {
//     return (dispatch, getState) => {
//         const productList = getState().products.allProducts;
//         const cartItems = getState().cart.cartItems;
//         const isItemPresent = cartItems.find(item => item.id === id)
//         let state;
//         if (!isItemPresent) {
//             const currentProduct = productList.find(product => product.id === id);
//             state = [...cartItems,{ ...currentProduct, count: 1 }]
//         } else {
//             state = cartItems.map((item)=>{
//                 if(item.id === id){
//                     item.count = operation === 'add' ? item.count + 1 : item.count - 1;
//                 }
//                 return item;
//             }).filter(({count}) => count)
//         }
//         api.post('/addToCart',id)
//         .then(res=>{
//             if(res){
//                 dispatch({
//                     type: actions.UPDATE_TO_CART,
//                     payload: state
//                 })
//             }
//         })

//     }
// }