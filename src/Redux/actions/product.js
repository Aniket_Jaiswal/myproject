import {api} from '../../helpers/axiosInstance';
import {actions} from '../actionTypes/actionConstants'

export const fetchProductType = () => {
    return (dispatch) => {
        api.get('/categories')
        .then(response=>{
            dispatch({
                type: actions.FETCH_PRODUCT_TYPE,
                payload: response.data,
            })
        })
    }
}

export const fetchAllProducts = () => {
    return(dispatch)=>{
        api.get('/products')
        .then(response=>{
            dispatch({
                type: actions.FETCH_ALL_PRODUCTS,
                payload: response.data,
            })
        })
    }
}