import {actions} from '../actionTypes/actionConstants';
import {api} from '../../helpers/axiosInstance';

export const fetchCarauselItems = () => {
    return (dispatch) => {
        api.get('/banners')
        .then(res=>{
            if(res){
                dispatch({
                    type: actions.FETCH_CARAUSEL_ITEMS,
                    payload: res.data,
                })
            }
        })
    }
}