import './App.css';
import React, { lazy, Suspense } from 'react';
import { Switch, Route } from 'react-router-dom';

import Header from './Containers/Header/header';
import HomePage from './Containers/HomePage/homePage';

function App(props) {
  const ProductsPage = lazy(() => import('./Containers/ProductsPage/productPage.js'));
  const SignInPage = lazy(() => import('./Containers/SignIn/signin.js'));
  const SignUpPage = lazy(() => import('./Containers/SignUp/signup.js'));
  const CartPage = lazy(() => import('./Containers/CartPage/CartPage.js'));

  return (
    <div className="App">
      <Header />
      <Suspense fallback='...loading'>
        <Switch>
          <Route path='/' exact component={HomePage} />
          <Route path='/products' exact component={ProductsPage} />
          <Route path='/products/:id' exact component={ProductsPage} />
          <Route path='/signin' exact component={SignInPage} />
          <Route path='/register' exact component={SignUpPage} />
          <Route path='/cart' exact component={CartPage} />
        </Switch>
      </Suspense>

    </div>
  );
}

export default App;

