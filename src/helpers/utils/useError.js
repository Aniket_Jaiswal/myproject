import { useState } from 'react';

export default function useError(){
    const [error, setError] = useState({});

    const handleError = (e, regex) => {
        const name = e.target.name;
        const value = e.target.value;
        const isMatch = value.match(regex);

        
        if(value && regex){
            isMatch ? setError({
                ...error,
                [name] : '',
            }):setError({
                ...error,
                [name] : `${[name]} invalid`
            })
        }else{
            setError({
                ...error,
                [name] : `${[name]} required`,
            })
        }
    
    }
    
    return{
        error,
        handleError,
    }
}