import { useState } from 'react';

export default function usePopUp(){
    const [openPopUp, setOpenPopup] = useState(false);

    const handlePopUp = (action) => {
        action ? setOpenPopup(true) : setOpenPopup(false);
    }
    console.log(openPopUp,'openPopUpHook')

    return{
        handlePopUp,
        openPopUp,
    }
}